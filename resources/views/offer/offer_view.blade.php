@include('includes.nav')

<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="service-view">
                    <div class="service-header mb-0">
                        <h1>{{$offer_data->name}}</h1>
                    </div>
                    <div class="service-cate mb-1">
                        <a href="" class="bg-danger">{{$offer_data->offer_value}} % OFF</a>
                    </div>
                    <?php
                    $image = json_decode($offer_data->images);
                    ?>
                    <div class="service-images service-carousel">
                        <div class="images-carousel owl-carousel owl-theme" id="related">
                            @if($image==new stdClass() || $image==null)
                                <img class="img-fluid serv-img" alt="service_image"
                                     src="/assets/img/default/default.png">
                            @else
                                @foreach($image as $img)
                                    <?php
                                    $img_path = "http://localhost/WEB-php/uploads/images/{$img}/560_560.jpeg";
                                    if (!file_exists('C:/xampp/htdocs/WEB-php/uploads/images/' . $img . '/560_560.jpeg')) {
                                        $img_path = "http://localhost/WEB-php/uploads/images/{$img}/560_560.png";
                                    }
                                    ?>
                                    <img alt="Service Image" src="{{$img_path}}">
                                @endforeach
                            @endif
                        </div>
                    </div>

                    <div class="service-details">
                        <div class="tab-content">
                            <div class="tab-pane fade show active" id="pills-home" role="tabpanel"
                                 aria-labelledby="pills-home-tab">
                                <div class="card service-description">
                                    <div class="card-body">
                                        <h5 class="card-title">Offer Description</h5>
                                        <p class="mb-0">{{$offer_data->description}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <h4 class="card-title">Related Offers</h4>
                <div class="service-carousel">
                    <div class="popular-slider owl-carousel owl-theme">
                        @if(count($related_offers)>0)
                            @foreach($related_offers as $related_offer)
                                <?php
                                $image = json_decode($related_offer->images);
                                ?>
                                <div class="service-widget">
                                    <div class="service-img">
                                        <a href="">
                                            @if($image==new stdClass() || $image==null)
                                                <img class="img-fluid serv-img" alt="Service Image"
                                                     src="/assets/img/default/default.png">
                                            @else
                                                @foreach($image as $img)
                                                    <?php
                                                    $img_path = "http://localhost/WEB-php/uploads/images/{$img}/560_560.jpeg";
                                                    if (!file_exists('C:/xampp/htdocs/WEB-php/uploads/images/' . $img . '/560_560.jpeg')) {
                                                        $img_path = "http://localhost/WEB-php/uploads/images/{$img}/560_560.png";
                                                    }
                                                    ?>
                                                    <img alt="Service Image" style="height: 230px" src="{{$img_path}}">
                                                    @break
                                                @endforeach
                                            @endif
                                        </a>
                                        @if($related_offer->value_type=="percent")
                                            <div class="item-info">
                                                <div class="cate-list"><a class="bg-yellow"
                                                                          href="">{{$related_offer->offer_value}} %
                                                        OFF</a></div>
                                            </div>
                                        @else
                                            <div class="item-info">
                                                <div class="cate-list"><a class="bg-yellow"
                                                                          href="">{{$related_offer->offer_value}}
                                                        {{$related_offer->currency}} OFF</a></div>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="service-content">
                                        <h3 class="title">
                                            <a href="">{{$related_offer->name}}</a>
                                        </h3>
                                        <p>Valid Till : {{date('d-m-Y', strtotime($related_offer->date_end))}}</p>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <p class="text-muted mb-1">No offers found with this store.</p>
                        @endif
                    </div>
                </div>
            </div>

            <div class="col-lg-4 theiaStickySidebar">
                <div class="card provider-widget clearfix">
                    <div class="card-body">
                        <h5 class="card-title">Store Information</h5>
                        <div class="about-author">
                            <div class="about-provider-img">
                                <div class="provider-img-wrap">
                                    <?php
                                    $image = json_decode($offer_data->store_images);
                                    ?>
                                    @if($image==new stdClass() || $image==null)
                                        <img class="img-fluid serv-img" alt="store_image"
                                             src="/assets/img/default/default.png">
                                    @else
                                        @foreach($image as $img)
                                            <?php
                                            $img_path = "http://localhost/WEB-php/uploads/images/{$img}/560_560.jpeg";
                                            if (!file_exists('C:/xampp/htdocs/WEB-php/uploads/images/' . $img . '/560_560.jpeg')) {
                                                $img_path = "http://localhost/WEB-php/uploads/images/{$img}/560_560.png";
                                            }
                                            ?>
                                            <img alt="store_image" height="100px" width="100px" src="{{$img_path}}">
                                            @break
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                            <div class="provider-details">
                                <h6 class="mb-1">{{$offer_data->store_name}}</h6>
                            </div>
                            <div class="provider-details">
                                <p class="text-muted mb-1">{{$offer_data->address}}</p>
                                <p class="small mb-1"></p>
                            </div>
                        </div>
                        <hr>
                        {{--                        <div class="provider-info">--}}
                        {{--                            <p class="mb-0"><i class="fas fa-phone-alt"></i>--}}
                        {{--                                {{}}--}}
                        {{--                            </p>--}}
                        {{--                        </div>--}}
                        <div class="service-book mt-2">
                            <a href="/view_shop/{{$offer_data->store_id}}" class="btn btn-primary"> Visit Store </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('includes.footer')
