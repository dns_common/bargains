<?php

/*$this->db->select_min('service_amount');
$this->db->from('services');
$min_price = $this->db->get()->row_array();

$this->db->select_max('service_amount');
$this->db->from('services');
$max_price = $this->db->get()->row_array();

$currency = currency_conversion(settings('currency'));


$query = $this->db->query("select * from system_settings WHERE status = 1");
$result = $query->result_array();
if(!empty($result))
{
    foreach($result as $data){
        if($data['key'] == 'currency_option'){
            $currency_option = $data['value'];
        }

    }
}*/
?>
@include('includes.nav')

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet"/>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<style>
    .form-control {
        font-size: 15px;
        min-height: 32px;
        font-weight: 551;

    }

    .img-fluid {
        height: 250px;
    }

    .select2-container--default .select2-selection--single {
        background-color: #fff;
        border: 1px solid #aaa;
        border-radius: 4px;
        opacity: 0.6;
        padding: 4px;
        height: 35px;
    }

    .select2-container--default .select2-selection--single .select2-selection__rendered {
        color: #000;
        font-weight: 551;
    }

    .page-item.active .page-link {
        background-color: #ff0080;
        border-color: #ff0080;
        border-radius: 50%;
    }

    .page-link {
        color: #ff0080;
        border-radius: 50%;
    }
</style>
<div class="breadcrumb-bar">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="breadcrumb-title">
                    <h2>Find The Best Stores</h2>
                </div>
            </div>
            <div class="col-auto float-right ml-auto breadcrumb-menu">
                <nav aria-label="breadcrumb" class="page-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php //echo base_url();?>">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Offers</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3 theiaStickySidebar">
                <div class="card filter-card">
                    <div class="card-body">
                        <h4 class="card-title mb-4">Search Filter</h4>
                        <form id="search_form" action="{{route('store_search_page')}}" method="post">
                            @csrf
                            <div class="filter-widget">
                                <div class="filter-list">
                                    <h4 class="filter-title">Keyword</h4>
                                    <input type="text" id="common_search" name="offer_search"
                                           class="form-control common_search" placeholder="What are you looking for?"/>
                                </div>
                                <div class="filter-list">
                                    <h4 class="filter-title">Sort By</h4>
                                    <select name="sort_by" id="sort_by"
                                            class="js-example-basic-single js-states form-control">
                                        <option value="">Sort By</option>
                                        <option value="1">Top Rated</option>
                                        <option value="2">Newest</option>
                                    </select>
                                </div>
                                {{--                                <div class="filter-list">--}}
                                {{--                                    <h4 class="filter-title">Location</h4>--}}
                                {{--                                    <select id="location" class="js-example-basic-single js-states form-control">--}}
                                {{--                                        <option value="">All Locations</option>--}}
                                {{--                                        @foreach($category_list as $list)--}}
                                {{--                                            <option value="{{$list->id_category}}">{{$list->name}}</option>--}}
                                {{--                                        @endforeach--}}
                                {{--                                    </select>--}}
                                {{--                                </div>--}}
                            </div>
                            <input type="submit" class="btn btn-primary pl-5 pr-5 btn-block get_services" name="search"
                                   value="Search Offer"/>
                        </form>
                    </div>
                </div>
            </div>


            <div class="col-lg-9">

                <div class="row align-items-center mb-4">
                    <div class="col-md-6 col">
                        <h4><span id="service_count">{{$count}}</span> Offers Found</h4>
                    </div>
                    <div class="col-md-6 col-auto">
                        <div class="view-icons ">
                            <a href="javascript:void(0);" class="grid-view active"><i class="fas fa-th-large"></i></a>
                        </div>

                    </div>
                </div>
                <div>
                    @if(isset($rate_list))
                        <div class="row" id="dataList">
                            @foreach ($rate_list as $r_list)
                                <?php
                                $store_list = \Illuminate\Support\Facades\DB::table('store')
                                    ->select('store.*')
                                    ->where('store.id_store', '=', $r_list->store_id)
                                    ->paginate(9);
                                ?>
                                @if(isset($store_list))
                                    @foreach($store_list as $list)
                                        <div class="col-lg-4 col-md-6">
                                            <div class="service-widget">
                                                <div class="service-img">
                                                    <a href="view_shop/{{$list->id_store}}">
                                                        <?php
                                                        $images = DB::table('store')
                                                            ->select('store.images')
                                                            ->where('store.id_store', '=', $list->id_store)
                                                            ->first();

                                                        $image = json_decode($images->images);
                                                        ?>
                                                        @if(isset($image))
                                                            @foreach($image as $img)
                                                                <?php
                                                                $img_path = "http://localhost/WEB-php/uploads/images/{$img}/560_560.jpeg";
                                                                if (!file_exists('C:/xampp/htdocs/WEB-php/uploads/images/' . $img . '/560_560.jpeg')) {
                                                                    $img_path = "http://localhost/WEB-php/uploads/images/{$img}/560_560.png";
                                                                }
                                                                ?>
                                                                <img class="img-fluid serv-img" alt="Service Image"
                                                                     src="{{$img_path}}">
                                                                @break
                                                            @endforeach
                                                        @else
                                                            <img class="img-fluid serv-img" alt="Service Image"
                                                                 src="assets/img/default/default.png">
                                                        @endif
                                                    </a>
                                                    <div class="item-info">
                                                        <div class="service-user">
                                                            <a href="view_shop/{{$list->id_store}}">

                                                                <?php
                                                                $images = DB::table('store')
                                                                    ->select('store.images')
                                                                    ->where('store.id_store', '=', $list->id_store)
                                                                    ->first();

                                                                $image = json_decode($images->images);

                                                                ?>

                                                                @if(isset($image))
                                                                    @foreach($image as $img)
                                                                        <?php
                                                                        $img_path = "http://localhost/WEB-php/uploads/images/{$img}/560_560.jpeg";
                                                                        if (!file_exists('C:/xampp/htdocs/WEB-php/uploads/images/' . $img . '/560_560.jpeg')) {
                                                                            $img_path = "http://localhost/WEB-php/uploads/images/{$img}/560_560.png";
                                                                        }
                                                                        ?>
                                                                        <img class="img-fluid serv-img" alt=""
                                                                             src="{{$img_path}}">
                                                                        @break
                                                                    @endforeach
                                                                    @else
                                                                        <img class="img-fluid serv-img" alt="Service Image"
                                                                             src="assets/img/default/default.png">
                                                                @endif

                                                            </a>
                                                            <span class="service-price">{{$list->name}}</span>
                                                        </div>
                                                        {{--                                            <div class="cate-list"> <a class="bg-yellow" href="">{{$list->category_name}}</a></div>--}}
                                                    </div>
                                                </div>
                                                <div class="service-content">
                                                    {{--                                        <h3 class="title">--}}
                                                    {{--                                            <a href="view_shop/{{$list->store_id}}">{{$list->name}}</a>--}}
                                                    {{--                                        </h3>--}}
                                                    <div class="rating">

                                                        <?php

                                                        $rate = \Illuminate\Support\Facades\DB::table('rate')
                                                            ->select(\Illuminate\Support\Facades\DB::raw("SUM(rate.rate) as rate"))
                                                            ->where('rate.store_id', '=', $list->id_store)
                                                            ->first();

                                                        $count = \Illuminate\Support\Facades\DB::table('rate')
                                                            ->select('rate.rate')
                                                            ->where('rate.store_id', '=', $list->id_store)
                                                            ->get();

                                                        $count = count($count);

                                                        if ($count == '') {
                                                            $count = 1;
                                                        }
                                                        if ($rate->rate == '') {
                                                            $rate->rate = 1;
                                                        }

                                                        $final_rate = $rate->rate / $count;

                                                        ?>
                                                        <?php
                                                        for ($x = 1; $x <= $final_rate; $x++) {
                                                            echo '<i class="fas fa-star filled"></i>';
                                                        }
                                                        if (strpos($final_rate, '.')) {
                                                            echo '<i class="fas fa-star"></i>';
                                                            $x++;
                                                        }
                                                        while ($x <= 5) {
                                                            echo '<i class="fas fa-star"></i>';
                                                            $x++;
                                                        }
                                                        ?>


                                                        <span
                                                            class="d-inline-block average-rating">{{$final_rate}}</span>
                                                    </div>
                                                    <div class="user-info">

                                                        <div class="row">

                                                <span class="col ser-contact"><i
                                                        class="fas fa-phone mr-1"></i> <span>{{$list->telephone}}</span></span>

                                                            <span
                                                                class="col ser-location"><span>{{$list->address}}</span> <i
                                                                    class="fas fa-map-marker-alt ml-1"></i></span>


                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    @endforeach
                                    <div class="row mt-3 mb-2">
                                        <div class="col text-center">
                                            <div class="block-27">
                                                <ul>
                                                    <li>  {{ $store_list->links() }} </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>


                    @else
                        <div class="row" id="dataList">
                            @foreach($store_list as $list)
                                <div class="col-lg-4 col-md-6">
                                    <div class="service-widget">
                                        <div class="service-img">
                                            <a href="view_shop/{{$list->id_store}}">
                                                <?php
                                                $images = DB::table('store')
                                                    ->select('store.images')
                                                    ->where('store.id_store', '=', $list->id_store)
                                                    ->first();

                                                $image = json_decode($images->images);
                                                ?>
                                                @if($image==new stdClass())
                                                        <img class="img-fluid serv-img" alt="Service Image"
                                                             src="assets/img/default/default.png">

                                                @else
                                                        @foreach($image as $img)
                                                            <?php
                                                            $img_path = "http://localhost/WEB-php/uploads/images/{$img}/560_560.jpeg";
                                                            if (!file_exists('C:/xampp/htdocs/WEB-php/uploads/images/' . $img . '/560_560.jpeg')) {
                                                                $img_path = "http://localhost/WEB-php/uploads/images/{$img}/560_560.png";
                                                            }

                                                            ?>
                                                            <img class="img-fluid serv-img" alt="Service Image"
                                                                 src="{{$img_path}}">
                                                            @break
                                                        @endforeach
                                                @endif
                                            </a>
                                            <div class="item-info">
                                                <div class="service-user">
                                                    <a href="view_shop/{{$list->id_store}}">

                                                        <?php
                                                        $images = DB::table('store')
                                                            ->select('store.images')
                                                            ->where('store.id_store', '=', $list->id_store)
                                                            ->first();

                                                        $image = json_decode($images->images);

                                                        ?>

                                                        @if(isset($image))
                                                            @foreach($image as $img)
                                                                <?php
                                                                $img_path = "http://localhost/WEB-php/uploads/images/{$img}/560_560.jpeg";
                                                                if (!file_exists('C:/xampp/htdocs/WEB-php/uploads/images/' . $img . '/560_560.jpeg')) {
                                                                    $img_path = "http://localhost/WEB-php/uploads/images/{$img}/560_560.png";
                                                                }
                                                                ?>
                                                                <img class="img-fluid serv-img" alt=""
                                                                     src="{{$img_path}}">
                                                                @break
                                                            @endforeach
                                                            @else
                                                                <img alt="Service Image" src="assets/img/default/default.png">
                                                        @endif
                                                    </a>
                                                    <span class="service-price">{{$list->name}}</span>
                                                </div>
                                                {{--                                            <div class="cate-list"> <a class="bg-yellow" href="">{{$list->category_name}}</a></div>--}}
                                            </div>
                                        </div>
                                        <div class="service-content">
                                            {{--                                        <h3 class="title">--}}
                                            {{--                                            <a href="view_shop/{{$list->store_id}}">{{$list->name}}</a>--}}
                                            {{--                                        </h3>--}}
                                            <div class="rating">

                                                <?php

                                                $rate = \Illuminate\Support\Facades\DB::table('rate')
                                                    ->select(\Illuminate\Support\Facades\DB::raw("SUM(rate.rate) as rate"))
                                                    ->where('rate.store_id', '=', $list->id_store)
                                                    ->first();

                                                $count = \Illuminate\Support\Facades\DB::table('rate')
                                                    ->select('rate.rate')
                                                    ->where('rate.store_id', '=', $list->id_store)
                                                    ->get();

                                                $count = count($count);

                                                if ($count == '') {
                                                    $count = 1;
                                                }
                                                if ($rate->rate == '') {
                                                    $rate->rate = 0;
                                                }

                                                $final_rate = $rate->rate / $count;


                                                ?>
                                                <?php
                                                for ($x = 1; $x <= $final_rate; $x++) {
                                                    echo '<i class="fas fa-star filled"></i>';
                                                }
                                                if (strpos($final_rate, '.')) {
                                                    echo '<i class="fas fa-star"></i>';
                                                    $x++;
                                                }
                                                while ($x <= 5) {
                                                    echo '<i class="fas fa-star"></i>';
                                                    $x++;
                                                }
                                                ?>


                                                <span class="d-inline-block average-rating">{{$final_rate}}</span>
                                            </div>
                                            <div class="user-info">

                                                <div class="row">

                                                <span class="col ser-contact"><i
                                                        class="fas fa-phone mr-1"></i> <span>{{$list->telephone}}</span></span>

                                                    <span class="col ser-location"><span>{{$list->address}}</span> <i
                                                            class="fas fa-map-marker-alt ml-1"></i></span>


                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endif
                    @if(isset($store_list))
                        <div class="row mt-3 mb-2">
                            <div class="col text-center">
                                <div class="block-27">
                                    <ul>
                                        <li>  {{ $store_list->links() }} </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endif

                </div>

            </div>
        </div>


    </div>
</div>
<script>
    if (window.history.replaceState) {
        window.history.replaceState(null, null, window.location.href);
    }
</script>
<script>
    $(document).ready(function () {
        $('.js-example-basic-single').select2();
    });
</script>
@include('includes.footer')
