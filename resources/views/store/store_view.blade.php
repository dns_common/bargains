@include('includes.nav')
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">

                <div class="service-view">
                    <div class="service-header">
                        <h1>{{$store->name}}</h1>
                        <address class="service-location"><i class="fas fa-location-arrow"></i> {{$store->address}}
                            ,{{$store->country}}</address>
                        <div class="rating">
                            @for ($x = 1; $x <= $avg_rating; $x++)
                                <i class="fas fa-star filled"></i>
                            @endfor

                            @if (strpos($avg_rating, '.'))
                                <i class="fas fa-star"></i>
                            @endif

                            {{--                            @while ($x <= 5)--}}
                            {{--                                <i class="fas fa-star"></i>--}}
                            {{--                                {{$x++}}--}}
                            {{--                            @endwhile--}}
                            @if($avg_rating>0)
                                <span class="d-inline-block average-rating">{{$avg_rating}}</span>
                            @endif
                        </div>
                        {{--                        <div class="service-cate">--}}
                        {{--                            <a href=""></a>--}}
                        {{--                        </div>--}}
                    </div>

                    <div class="service-images service-carousel">
                        <div class="images-carousel owl-carousel owl-theme owl-rtl owl-loaded owl-drag">
                            <?php
                            $store_image = json_decode($store->images);
                            ?>

                            @if($store_image==new stdClass()||$store_image=='')
                                <img alt="store_image"
                                     src="/assets/img/default/default.png">
                            @else
                                @foreach($store_image as $img)
                                    <?php
                                    $img_path = "http://localhost/WEB-php/uploads/images/{$img}/560_560.jpeg";
                                    if (!file_exists('C:/xampp/htdocs/WEB-php/uploads/images/' . $img . '/560_560.jpeg')) {
                                        $img_path = "http://localhost/WEB-php/uploads/images/{$img}/560_560.png";
                                    }
                                    ?>
                                    <img alt="Service Image" src="{{$img_path}}">
                                @endforeach
                            @endif
                        </div>
                    </div>


                    <div class="service-details">
                        <ul class="nav nav-pills service-tabs" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home"
                                   role="tab" aria-controls="pills-home" aria-selected="true">Overview</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-book-tab" data-toggle="pill" href="#pills-book" role="tab"
                                   aria-controls="pills-book" aria-selected="false">Reviews</a>
                            </li>
                        </ul>

                        <div class="tab-content">

                            <div class="tab-pane fade show active" id="pills-home" role="tabpanel"
                                 aria-labelledby="pills-home-tab">
                                <div class="card service-description">
                                    <div class="card-body">
                                        <h5 class="card-title">Store Details</h5>
                                        <p class="mb-0">{{$store->detail}}</p>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="pills-book" role="tabpanel" aria-labelledby="pills-book-tab">
                                <div class="card review-box">
                                    <div class="card-body">
                                        @if(count($rate_data)>0)
                                            @foreach($rate_data as $rate_data)
                                                <div class="review-list">
                                                    <div class="review-img">
                                                        @if(isset($rate_data->images))
                                                            <?php
                                                            $img_path = "http://localhost/WEB-php/uploads/images/{$rate_data->images}/560_560.jpeg";
                                                            if (!file_exists('C:/xampp/htdocs/WEB-php/uploads/images/' . $rate_data->images . '/560_560.jpeg')) {
                                                                $img_path = "http://localhost/WEB-php/uploads/images/{$rate_data->images}/560_560.png";
                                                            }
                                                            ?>
                                                            <img alt="Service Image" src="{{$img_path}}">
                                                            @break
                                                        @else
                                                            <img alt="store_image"
                                                                 src="/assets/img/default/default.png">
                                                        @endif
                                                    </div>
                                                    <div class="review-info">
                                                        <h5>{{$rate_data->name}}</h5>
                                                        <div
                                                            class="review-date">{{$rate_data->created_at}}</div>
                                                        <p class="mb-0">{{$rate_data->review}}</p>
                                                    </div>
                                                    <div class="review-count">
                                                        <div class="rating">
                                                            <span class="d-inline-block average-rating">({{$rate_data->rate}})</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @else
                                            <span>No reviews found</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

                <h4 class="card-title">Store Offers</h4>
                <div class="service-carousel">
                    <div class="popular-slider owl-carousel owl-theme">
                        @if(count($offers)>0)
                            @foreach($offers as $offer)
                                <div class="service-widget" style="height: 335px">
                                    <div class="service-img">
                                        <a href="/view_offer/{{$offer->id_offer}}">
                                            <?php
                                            $offer_image = json_decode($offer->images);
                                            ?>
                                            @if($offer_image==new stdClass()||$offer_image=='')
                                                <img alt="store_image"
                                                     src="/assets/img/default/default.png">
                                            @else
                                                @foreach($offer_image as $img)
                                                    <?php

                                                    $img_path = "http://localhost/WEB-php/uploads/images/{$img}/560_560.jpeg";
                                                    if (!file_exists('C:/xampp/htdocs/WEB-php/uploads/images/' . $img . '/560_560.jpeg')) {
                                                        $img_path = "http://localhost/WEB-php/uploads/images/{$img}/560_560.png";
                                                    }
                                                    ?>
                                                    <img style="height: 230px" alt="Service Image" src="{{$img_path}}">
                                                    @break
                                                @endforeach
                                            @endif
                                        </a>
                                        <div class="item-info">
                                            <div class="cate-list"><a class="bg-yellow" href="">{{$offer->offer_value}}%
                                                    OFF</a></div>
                                        </div>
                                    </div>
                                    <div class="service-content">
                                        <h3 class="title">
                                            <a href="/view_offer/{{$offer->id_offer}}">{{$offer->name}}</a>
                                        </h3>
                                        <p>{{$offer->description}}</p>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <p class="text-muted mb-1">No offers found with this store.</p>
                        @endif
                    </div>
                </div>
            </div>

            <div class="col-lg-4 theiaStickySidebar">
                <div class="card provider-widget clearfix">
                    <div class="card-body">
                        <h5 class="card-title">Store Information</h5>
                        <div class="about-author">
                            <div class="about-provider-img">
                                <div class="provider-img-wrap">
                                    <a href="javascript:void(0);">
                                        <?php
                                        $store_image = json_decode($store->images);
                                        ?>
                                        @if($store_image==new stdClass())
                                            <img alt="store_image"
                                                 src="/assets/img/default/default.png">
                                        @else
                                            @foreach($store_image as $img)
                                                <?php
                                                $img_path = "http://localhost/WEB-php/uploads/images/{$img}/100_100.jpeg";
                                                if (!file_exists('C:/xampp/htdocs/WEB-php/uploads/images/' . $img . '/100_100.jpeg')) {
                                                    $img_path = "http://localhost/WEB-php/uploads/images/{$img}/100_100.png";
                                                }
                                                ?>
                                                <img class="img-fluid rounded-circle" alt="service_image"
                                                     src="{{$img_path}}"></a>
                                    @break
                                    @endforeach
                                    @endif
                                </div>
                            </div>
                            <div class="provider-details">
                                <h6 class="mb-1">{{$store->name}}</h6>
                            </div>
                            <div class="provider-details">
                                <p class="text-muted mb-1">Member Since</p>
                                <p class="small mb-1">{{date('d-m-Y',strtotime($store->created_at))}}</p>
                            </div>
                        </div>
                        <hr>
                        <div class="provider-info">
                            <p class="mb-0"><i class="fas fa-phone-alt"></i>
                                {{$store->telephone}}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="card available-widget">
                    <div class="card-body">
                        <h5 class="card-title">Open Hours</h5>
                        <ul>
                            @if(count($open_days)>0)
                                @foreach($open_days as $open_day)
                                    @if($open_day->enabled==1)
                                        <li><span>{{strtoupper($open_day->day)}}</span> {{$open_day->opening}}
                                            - {{$open_day->closing}}</li>
                                    @endif
                                @endforeach
                            @else
                                <p class="text-muted mb-1">No data available</p>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('includes.footer')
