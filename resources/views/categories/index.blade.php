@include('includes.nav')
<style>
    .footer {
        background-color: #fff;
        border-top: 0px solid #e8ecec;
        margin-top: 20px;
    }
     .cate-widget img
     {
         height: 250px;
     }

    .page-item.active .page-link {
        background-color: #ff0080;
        border-color: #ff0080;
        border-radius: 50%;
    }
    .page-link {
        color: #ff0080;
        border-radius: 50%;
    }

</style>
<div class="breadcrumb-bar">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="breadcrumb-title">
                    <h2>Categories</h2>
                </div>
            </div>
            <div class="col-auto float-right ml-auto breadcrumb-menu">
                <nav aria-label="breadcrumb" class="page-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Categories</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

<div class="content">
    <div class="container">
        <div class="">

        </div>
        <div class="catsec">
                <div class="row" id="dataList">
                    @foreach($category_list as $list)
                        <div class="col-lg-4 col-md-6">
                            <a href="{{route('search')}}">
                                <div class="cate-widget">
                                    <img src='http://localhost/WEB-php/uploads/images/{{$list->image}}/560_560.jpeg'>
                                    <div class="cate-title">
                                        <h3><span><i class="fas fa-circle"></i> {{$list->name}}</span></h3>
                                    </div>

                                    <?php
                                    $count = \Illuminate\Support\Facades\DB::table('offer')
                                    ->select(\Illuminate\Support\Facades\DB::raw("count(offer.category_id) as count"))
                                    ->where('offer.category_id','=',$list->id_category)
                                    ->get();

                                    ?>
                                    @if(isset($count))
                                        <div class="cate-count">
                                            <i class="fas fa-clone">{{$count[0]->count}}</i>
                                        </div>
                                    @else
                                        <div class="cate-count">
                                            <i class="fas fa-clone">0</i>
                                        </div>
                                        @endif

                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
                    {{ $category_list->links() }}
        </div>
    </div>
</div>
@include('includes.footer')
