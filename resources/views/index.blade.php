@include('includes.nav')
<style>
    .cate-widget img {
        height: 250px;
    }

    #carousel1 {

    }
    .search-input.line::before {
        width: 0px;
    }


</style>
<section class="hero-section">
    <div class="layer">
        <div class="home-banner"></div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="section-search">
                        <h3>Largest <span> Bargain Place</span></h3>
                        <p>Search For Awesome Deals </p>
                        <div class="search-box">
                            <form action='{{route('search')}}' id="search_service" method="post">
                                @csrf
                                <input type="hidden" name="" value=""/>
                                <div class="search-input line">
                                    <i class="fas fa-tv bficon"></i>
                                    <div class="form-group mb-0">
                                        <input type="text" class="form-control common_search" name="offer_search"
                                               id="search-blk" placeholder="What are you looking for?">
                                    </div>
                                </div>

                                <div class="search-btn">
                                    <button class="btn search_service" name="search" value="search" type="button">
                                        Search
                                    </button>
                                </div>
                            </form>
                        </div>
                        {{--                        <div class="search-cat">--}}
                        {{--                            <i class="fas fa-circle"></i>--}}
                        {{--                            <span>Popular Searches</span>--}}
                        {{--                            --}}{{--                            popular searches--}}
                        {{--                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="popular-services">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-md-6">
                        <div class="heading">
                            <h2>Latest Promotions</h2>
                            <span>Explore the latest our Promotions. You won’t be disappointed</span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="viewall">
                            {{--                            <h4><a href="all-services">View All <i class="fas fa-angle-right"></i></a></h4>--}}
                            <span>Requested Slider</span>
                        </div>
                    </div>
                </div>
                <div class="service-carousel">
                    <div class="service-slider owl-carousel owl-theme" id="carousel1">
                        @foreach($banners as $banner)
                            <div class="service-widget">
                                <div class="service-img">
                                    <?php
                                    $image = json_decode($banner->image);
                                    ?>
                                    @if(isset($image))
                                        @foreach($image as $img)
                                            <?php
                                            $img_path = "http://localhost/WEB-php/uploads/images/{$img}/560_560.jpeg";
                                            if (!file_exists('C:/xampp/htdocs/WEB-php/uploads/images/' . $img . '/560_560.jpeg')) {
                                                $img_path = "http://localhost/WEB-php/uploads/images/{$img}/560_560.png";
                                                if (!file_exists("http://localhost/WEB-php/uploads/images/{$img}/560_560.png")) {
                                                    $img_path = "assets/img/default/default.png";

                                                }
                                            }
                                            ?>
                                            <a href="">
                                                <img class="img-fluid serv-img" alt="Service Image"
                                                     src="{{$img_path}}">
                                            </a>
                                            @break
                                        @endforeach
                                    @endif


                                </div>
                                <div class="service-content">
                                    <h3 class="title">
                                        <a href="">{{$banner->title}}</a>
                                    </h3>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="category-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-md-6">
                        <div class="heading">
                            <h2>Featured Categories</h2>
                            <span>What do you looking for?</span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="viewall">
                            <h4><a href="/categories">View All <i class="fas fa-angle-right"></i></a></h4>
                            <span>Featured Categories</span>
                        </div>
                    </div>
                </div>
                <div class="catsec">
                    <div class="row">
                        @if(count($categories)>0)
                            @foreach($categories as $category)
                                <div class="col-lg-4 col-md-6">
                                    <a href="/categories">
                                        <div class="cate-widget">
                                            @if(isset($category->image))
                                                <?php
                                                $img_path = "http://localhost/WEB-php/uploads/images/{$category->image}/560_560.jpeg";
                                                if (!file_exists('C:/xampp/htdocs/WEB-php/uploads/images/' . $category->image . '/560_560.jpeg')) {
                                                    $img_path = "http://localhost/WEB-php/uploads/images/{$category->image}/560_560.png";
                                                }
                                                ?>
                                                <img alt="service_image" src="{{$img_path}}">
                                            @endif
                                            <div class="cate-title">
                                                <h3><span><i class="fas fa-circle"></i>{{$category->name}}</span></h3>
                                            </div>
                                            <?php
                                            $count = \Illuminate\Support\Facades\DB::table('offer')
                                                ->select(\Illuminate\Support\Facades\DB::raw("count(offer.category_id) as count"))
                                                ->where('offer.category_id', '=', $category->id_category)
                                                ->get();

                                            ?>
                                            @if(isset($count))
                                                <div class="cate-count">
                                                    <i class="fas fa-clone">{{$count[0]->count}}</i>
                                                </div>
                                            @else
                                                <div class="cate-count">
                                                    <i class="fas fa-clone">0</i>
                                                </div>
                                            @endif
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        @else
                            <div class="col-lg-12">
                                <div class="category">
                                    No Categories Found
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="popular-services">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-md-6">
                        <div class="heading">
                            <h2>Recent Offers</h2>
                            <span>Explore the greatest our Offers. You won’t be disappointed</span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="viewall">
                            <h4><a href="{{route('search')}}">View All <i class="fas fa-angle-right"></i></a></h4>
                            <span>All Offers</span>
                        </div>
                    </div>
                </div>
                <div class="service-carousel">
                    <div class="service-slider owl-carousel owl-theme">

                        @if(!empty($offers))
                            @foreach($offers as $offer)
                                <?php
                                $image = json_decode($offer->offer_images);
                                ?>
                                <div class="service-widget">
                                    <div class="service-img">
                                        <a href="/view_offer/{{$offer->id_offer}}">
                                            @if(isset($image))
                                                @forelse($image as $img)
                                                    <?php
                                                    $img_path = "http://localhost/WEB-php/uploads/images/{$img}/560_560.jpeg";
                                                    if (!file_exists('C:/xampp/htdocs/WEB-php/uploads/images/' . $img . '/560_560.jpeg')) {
                                                        $img_path = "http://localhost/WEB-php/uploads/images/{$img}/560_560.png";
                                                    }
                                                    ?>
                                                    <img alt="Service Image" style="height: 230px" src="{{$img_path}}">
                                                    @break
                                                @empty
                                                    <img alt="Service Image" src="assets/img/default/default.png">
                                                @endforelse
                                            @endif
                                        </a>
                                        <div class="item-info">
                                            <div class="service-user">
                                                <a href="/view_shop/{{$offer->id_store}}">
                                                    <?php
                                                    $image = json_decode($offer->store_images);
                                                    ?>
                                                    @forelse($image as $img)
                                                        <?php
                                                        $img_path = "http://localhost/WEB-php/uploads/images/{$img}/560_560.jpeg";
                                                        if (!file_exists('C:/xampp/htdocs/WEB-php/uploads/images/' . $img . '/560_560.jpeg')) {
                                                            $img_path = "http://localhost/WEB-php/uploads/images/{$img}/560_560.png";
                                                        }
                                                        ?>
                                                        <img alt="Service Image"
                                                             src="{{$img_path}}">
                                                        @break
                                                    @empty
                                                        <img alt="Service Image"
                                                             src="assets/img/default/default.png">
                                                    @endforelse
                                                </a>
                                                <span
                                                    class="service-price">{{$offer->store_name}}</span>
                                            </div>
                                            @if($offer->value_type=="percent")
                                                <div class="item-info">
                                                    <div class="cate-list"><a class="bg-yellow"
                                                                              href="">{{$offer->offer_value}} %
                                                            OFF</a></div>
                                                </div>
                                            @else
                                                <div class="item-info">
                                                    <div class="cate-list"><a class="bg-yellow"
                                                                              href="">{{$offer->offer_value}}
                                                            {{$offer->currency}} OFF</a></div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="service-content" style="height: 80px">
                                        <h3 class="title">
                                            <a href="/view_offer/{{$offer->id_offer}}">{{$offer->offer_name}}</a>
                                        </h3>
                                        @if($offer->is_deal==1)
                                            <div class="rating">
                                                <span
                                                    class="d-inline-block average-rating">Valid Till : {{date('d-m-Y', strtotime($offer->date_end))}}</span>
                                            </div>
                                        @endif
                                        {{--                                        <div class="user-info">--}}

                                        {{--                                            <div class="row">--}}

                                        {{--                                        <span class="col ser-contact"><i--}}
                                        {{--                                                class="fas fa-phone mr-1"></i> <span>{{$offer->telephone}}</span></span>--}}
                                        {{--                                                <span--}}
                                        {{--                                                    class="col ser-location"><span>{{$offer->city}},{{$offer->country}}</span> <i--}}
                                        {{--                                                        class="fas fa-map-marker-alt ml-1"></i></span>--}}
                                        {{--                                            </div>--}}
                                        {{--                                        </div>--}}
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div>
                                <p class="mb-0">
                                    No Offers Found
                                </p>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="how-work">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="heading howitworks">
                    <h2>How It Works</h2>
                    <span>Aliquam lorem ante, dapibus in, viverra quis</span>
                </div>
                <div class="howworksec">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="howwork">
                                <div class="iconround">
                                    <div class="steps">01</div>
                                    <img src="assets/img/icon-1.png">
                                </div>
                                <h3>Explore The Best Deals</h3>
                                <p>Find the best deals around your location. Type what you need and search.</p>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="howwork">
                                <div class="iconround">
                                    <div class="steps">02</div>
                                    <img src="assets/img/icon-2.png">
                                </div>
                                <h3>Post Your Deals</h3>
                                <p>Register with us and make your own shop. Post your deals trough it. </p>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="howwork">
                                <div class="iconround">
                                    <div class="steps">03</div>
                                    <img src="assets/img/icon-3.png">
                                </div>
                                <h3>Find The Best</h3>
                                <p>Find the best offers that makes you most satisfy.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="popular-services">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-md-6">
                        <div class="heading">
                            <h2>Recently added stores</h2>
                            <span>Explore the greatest our stores. You won’t be disappointed</span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="viewall">
                            <h4><a href='{{route('stores')}}'>View All <i class="fas fa-angle-right"></i></a></h4>
                            <span>All Stores</span>
                        </div>
                    </div>
                </div>
                <div class="service-carousel">
                    <div class="service-slider owl-carousel owl-theme">
                        @if(!empty($stores))
                            @foreach($stores as $store)
                                <?php
                                $store_image = json_decode($store->images);
                                ?>
                                <div class="service-widget">
                                    <div class="service-img">
                                        <a href="/view_shop/{{$store->id_store}}">
                                            @forelse($store_image as $img)
                                                <?php
                                                $img_path = "http://localhost/WEB-php/uploads/images/{$img}/560_560.jpeg";
                                                if (!file_exists('C:/xampp/htdocs/WEB-php/uploads/images/' . $img . '/560_560.jpeg')) {
                                                    $img_path = "http://localhost/WEB-php/uploads/images/{$img}/560_560.png";
                                                }
                                                ?>
                                                <img alt="Service Image" style="height: 230px" src="{{$img_path}}">
                                                @break
                                            @empty
                                                <img alt="Service Image" style="height: 230px"
                                                     src="assets/img/default/default.png">
                                            @endforelse
                                        </a>
                                    </div>
                                    <div class="service-content">
                                        <h3 class="title">
                                            <a href="/view_shop/{{$store->id_store}}">{{$store->name}}</a>
                                        </h3>
                                        <div class="rating">
                                            <?php

                                            $rate = \Illuminate\Support\Facades\DB::table('rate')
                                                ->select(\Illuminate\Support\Facades\DB::raw("SUM(rate.rate) as rate"))
                                                ->where('rate.store_id', '=', $store->id_store)
                                                ->first();

                                            $count = \Illuminate\Support\Facades\DB::table('rate')
                                                ->select('rate.rate')
                                                ->where('rate.store_id', '=', $store->id_store)
                                                ->get();

                                            $count = count($count);

                                            if ($count == '') {
                                                $count = 1;
                                            }
                                            if ($rate->rate == '') {
                                                $rate->rate = 0;
                                            }

                                            $final_rate = $rate->rate / $count;

                                            ?>
                                            <?php
                                            for ($x = 1; $x <= $final_rate; $x++) {
                                                echo '<i class="fas fa-star filled"></i>';
                                            }
                                            if (strpos($final_rate, '.')) {
                                                echo '<i class="fas fa-star"></i>';
                                                $x++;
                                            }
                                            while ($x <= 5) {
                                                echo '<i class="fas fa-star"></i>';
                                                $x++;
                                            }
                                            ?>

                                            <span class="d-inline-block average-rating">{{$final_rate}}</span>
                                        </div>
                                        <div class="user-info">
                                            <div class="row">
                                        <span class="col ser-contact"><i
                                                class="fas fa-phone mr-1"></i> <span>{{$store->telephone}}</span></span>
                                                <span
                                                    class="col ser-location"><span>{{$store->city}},{{$store->country}}</span> <i
                                                        class="fas fa-map-marker-alt ml-1"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div>
                                <p class="mb-0">
                                    No events Found
                                </p>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="popular-services">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-md-6">
                        <div class="heading">
                            <h2>Recent Events</h2>
                            <span>Explore the greates our services. You won’t be disappointed</span>
                        </div>
                    </div>
{{--                    <div class="col-md-6">--}}
{{--                        <div class="viewall">--}}
{{--                            <h4><a href="all-services">View All <i class="fas fa-angle-right"></i></a></h4>--}}
{{--                            <span>All Events</span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
                <div class="service-carousel">
                    <div class="service-slider owl-carousel owl-theme">
                        @if(count($events)>0)
                            @foreach($events as $event)
                                <?php
                                $image = json_decode($event->images);
                                $store_image = json_decode($event->store_image);
                                ?>
                                <div class="service-widget">
                                    <div class="service-img">
                                        <a href="#">
                                            @if(isset($image))
                                                @forelse($image as $img)
                                                    <?php
                                                    $img_path = "http://localhost/WEB-php/uploads/images/{$img}/560_560.jpeg";
                                                    if (!file_exists('C:/xampp/htdocs/WEB-php/uploads/images/' . $img . '/560_560.jpeg')) {
                                                        $img_path = "http://localhost/WEB-php/uploads/images/{$img}/560_560.png";
                                                    }
                                                    ?>
                                                    <img alt="Service Image" style="height: 230px" src="{{$img_path}}">
                                                    @break
                                                @empty
                                                    <img alt="Service Image" src="assets/img/default/default.png">
                                                @endforelse
                                            @endif
                                        </a>
                                        <div class="item-info">
                                            <div class="service-user">
                                                <a href="#">
                                                    @if(isset($store_image))
                                                        @foreach($store_image as $img)
                                                            <?php
                                                            $img_path = "http://localhost/WEB-php/uploads/images/{$img}/560_560.jpeg";
                                                            if (!file_exists('C:/xampp/htdocs/WEB-php/uploads/images/' . $img . '/560_560.jpeg')) {
                                                                $img_path = "http://localhost/WEB-php/uploads/images/{$img}/560_560.png";
                                                            }
                                                            ?>
                                                            <img alt="Service Image" src="{{$img_path}}">
                                                            @break
                                                        @endforeach
                                                    @endif
                                                </a>
                                                <span
                                                    class="service-price">{{$event->store_name}}</span>
                                            </div>
                                            <div class="cate-list">
                                                <a class="bg-yellow"
                                                   href="#">{{$event->date_b}}</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="service-content" style="height: 80px">
                                        <h3 class="title">
                                            <a href="#">{{$event->name}}</a>
                                        </h3>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div>
                                <p class="mb-0">
                                    No events Found
                                </p>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="{{asset('assets/plugins/owlcarousel/owl.carousel.min.js')}}"></script>
<script type="text/javascript">

    $('#carousel1').owlCarousel({
        margin: 20,
        responsive: {
            320: {items: 1},
            480: {items: 1},
            768: {items: 2},
            1024: {
                items: 2
            }
        },
    })

</script>

@include('includes.footer')
