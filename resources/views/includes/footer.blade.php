


<footer class="footer">

    <input type="hidden" id="stripe_key" value="">
    <input type="hidden" id="logo_front" value="">

    <!-- Footer Top -->
    <div class="footer-top">
        <div class="container">
            <div class="row">

                <div class="col-lg-3 col-md-6">

                    <!-- Footer Widget -->
                    <div class="footer-widget footer-menu">
                        <h2 class="footer-title">Quick Links  </h2>
                        <ul>
                            <li><a href="faq">FAQ</a></li>
                            <li><a href="help">Help</a></li>
                            <li><a href="javascript:void(0);" data-toggle="modal" data-target="#modal-wizard">Create Account</a></li>
                            <li><a href="/aboutUs">About Us</a></li>
                        </ul>
                    </div>
                    <!-- /Footer Widget -->

                </div>

{{--                <div class="col-lg-3 col-md-6">--}}

{{--                    <!-- Footer Widget -->--}}
{{--                    <div class="footer-widget footer-menu">--}}
{{--                        <h2 class="footer-title">Categories</h2>--}}
{{--                        <ul>--}}

{{--                            <li><a href=""></a></li>--}}

{{--                        </ul>--}}
{{--                    </div>--}}
{{--                    <!-- /Footer Widget -->--}}

{{--                </div>--}}

                <div class="col-lg-3 col-md-6">

                    <!-- Footer Widget -->
                    <div class="footer-widget footer-contact">
                        <h2 class="footer-title">Contact Us</h2>
                        <div class="footer-contact-info">
                            <div class="footer-address">
                                <span><i class="far fa-building"></i></span>
                                <p>
                                    221, Allen Ave, Dehiwala, Colombo
                                </p>
                            </div>
                            <p>
                                <i class="fas fa-phone"></i>
                                <a style="color: black" href="tel:+94703111111">0703-111-111</a>

                            </p>
                            <p class="mb-0">
                                <i class="fas fa-envelope"></i>
                                <a style="color: black" href="mailto: info@bargains.lk">info@bargains.lk</a>
                            </p>
                        </div>
                    </div>
                    <!-- /Footer Widget -->

                </div>
                <div class="col-lg-3 col-md-6">

                    <!-- Footer Widget -->
                    <div class="footer-widget">
                        <h2 class="footer-title">Follow Us</h2>
                        <div class="social-icon">
                            <ul>
                                <li>
                                    <a href="#" target="_blank"><i class="fab fa-facebook-f"></i> </a>
                                </li>
                                <li>
                                    <a href="#" target="_blank"><i class="fab fa-twitter"></i> </a>
                                </li>
                                <li>
                                    <a href="#" target="_blank"><i class="fab fa-youtube"></i></a>
                                </li>
                                <li>
                                    <a href="#" target="_blank"><i class="fab fa-google"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- /Footer Widget -->

                </div>

            </div>
        </div>
    </div>
    <!-- /Footer Top -->

    <!-- Footer Bottom -->
    <div class="footer-bottom">
        <div class="container">

            <!-- Copyright -->
            <div class="copyright">
                <div class="row">
                    <div class="col-md-6 col-lg-6">
                        <div class="copyright-text">
                            <p class="mb-0">&copy Design and developed by <a href="https://webtude.com/">Webtude</a></p>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6">

                        <!-- Copyright Menu -->
                        <div class="copyright-menu">
                            <ul class="policy-menu">
                                <li><a href="terms-conditions">Terms and Conditions</a></li>
                                <li><a href="privacy">Privacy</a></li>
                            </ul>
                        </div>
                        <!-- /Copyright Menu -->

                    </div>
                </div>
            </div>
            <!-- /Copyright -->

        </div>
    </div>
    <!-- /Footer Bottom -->

</footer>
</div>


<script src="{{asset('assets/js/moment.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap-datetimepicker.min.js')}}"></script>
<script src="{{asset('assets/js/popper.min.js')}}"></script>

<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/sweetalert.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/datatables.min.js')}}"></script>
<script src="{{asset('assets/js/cropper_profile_provider.js')}}"></script>
<script src="{{asset('assets/js/cropper.min.js')}}"></script>
<script src="{{asset('assets/js/script_crop.js')}}"></script>
<script src="{{asset('assets/js/bootstrapValidator.min.js')}}"></script>
<!-- Sticky Sidebar JS -->
<script src="{{asset('assets/plugins/theia-sticky-sidebar/ResizeSensor.js')}}"></script>
<script src="{{asset('assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js')}}"></script>
<!-- Toaster -->
<script src="{{asset('assets/plugins/toaster/toastr.min.js')}}"></script>
<script src="{{asset('assets/plugins/owlcarousel/owl.carousel.min.js')}}"></script>
<script src="{{asset('assets/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
<script src="{{asset('https://maps.googleapis.com/maps/api/js?key=AIzaSyDzviwvvZ_S6Y1wS6_b3siJWtSJ5uFQHoc&v=3.exp&libraries=places')}}"></script>

<input type="hidden" id="modules_page" value="">
<input type="hidden" id="current_page" value="">


<script src="{{asset('assets/js/bootstrap-select.min.js')}}"></script>
<script src="{{asset('assets/js/tagsinput.js')}}"></script>
<script src="{{asset('assets/js/service.js')}}"></script>
<script src="{{asset('assets/js/script.js')}}"></script>


<script src="{{asset('assets/js/multi-step-modal.js')}}"></script>
<link rel="stylesheet" href="{{asset('assets/plugins/jquery-confirm/jquery-confirm.min.css')}}">
<script src="{{asset('assets/plugins/jquery-confirm/jquery-confirm.min.js')}}"></script>
<script src="{{asset('assets/js/functions.js')}}"></script>
<input type="hidden" id="user_type" value="">


</body>

</html>
