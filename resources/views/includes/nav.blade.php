@include('includes.header')
<body>
{{--<div class="page-loading">--}}
{{--    <div class="preloader-inner">--}}
{{--        <div class="preloader-square-swapping">--}}
{{--            <div class="cssload-square-part cssload-square-green"></div>--}}
{{--            <div class="cssload-square-part cssload-square-pink"></div>--}}
{{--            <div class="cssload-square-blend"></div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
<div class="main-wrapper">
    <header class="header sticktop">
        <nav class="navbar navbar-expand-lg header-nav">
            <div class="navbar-header">
                <a id="mobile_btn" href="javascript:void(0);">
						<span class="bar-icon">
							<span></span>
							<span></span>
							<span></span>
						</span>
                </a>
                <a href="" class="navbar-brand logo">
                    <img src="/assets/img/logo.png" class="img-fluid" alt="Logo">
                </a>
                <a href="" class="navbar-brand logo-small">
                    <img src="/assets/img/logo.png" class="img-fluid" alt="Logo">
                </a>
            </div>
            <div class="main-menu-wrapper">
                <div class="menu-header">
                    <a href="" class="menu-logo">
                        <img src="" class="img-fluid" alt="Logo">
                    </a>
                    <a id="menu_close" class="menu-close" href="javascript:void(0);">
                        <i class="fas fa-times"></i>
                    </a>
                </div>
                <ul class="main-nav">
                    <li><a href="/">Home</a></li>
                    <li>
                        <a href="/search">Offers</a>
                    </li>
                    <li><a href="/stores">Stores</a></li>
                    <li><a href="/aboutUs">About Us</a></li>
                    <li>
                        <a class="nav-link header-login" href="http://localhost/WEB-php/index.php/user/login">Login</a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
</div>
