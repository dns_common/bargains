<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Routing\Route;

use Helpers;

class SearchController extends Controller
{
    public function search(Request $request)
    {
        $key = $request->offer_search;

        $offer_list = DB::table('offer')
            ->select('category.name as category_name','offer.*','store.name as store_name','store.telephone','store.address', 'offer.images','store.images as store_image')
            ->where(function($query) use ($key){
                return $query
                    ->Where('offer.name', 'like', '%' . $key . '%')
                    ->orWhere('offer.description', 'like', '%' . $key . '%');
            })
            ->leftJoin('category','offer.category_id','=','category.id_category')
            ->leftJoin('store','store.id_store','=','offer.store_id')
            ->Where('offer.status','=',1)
            ->orderBy('offer.created_at','desc')
            ->paginate(9);

        $count = count($offer_list);

        $category_list = DB::table('category')
            ->select('category.*')
            ->get();

        $districts_list = DB::table('districts')
            ->select('districts.*')
            ->get();

        return view('search',compact("offer_list","count","category_list",'districts_list'));
    }


    public function search_page(Request $request)
    {
        $key = $request->offer_search;

        $offer_list = DB::table('offer')
            ->select('category.name as category_name','offer.*','store.telephone','store.address', 'offer.images','store.images as store_image','store.name as store_name')
            ->where(function($query) use ($key){
                return $query
                    ->Where('offer.name', 'like', '%' . $key . '%')
                    ->orWhere('offer.description', 'like', '%' . $key . '%')
                    ->Where('store.address','like','%' . 'anuradhapura'  . '%');
            })
            ->Where('offer.status','=',1)
            ->leftJoin('category','offer.category_id','=','category.id_category')
            ->leftJoin('store','store.id_store','=','offer.store_id')
            ->paginate(9);


        $count = count($offer_list);

        $category_list = DB::table('category')
            ->select('category.*')
            ->get();

        if(isset($request->sort_by))
        {
            if($request->sort_by == 1)
            {
                $offer_list = DB::table('offer')
                    ->select('category.name as category_name','offer.*','store.telephone','store.address', 'offer.images','store.images as store_image','store.name as store_name')
                    ->where(function($query) use ($key){
                        return $query
                            ->Where('offer.name', 'like', '%' . $key . '%')
                            ->orWhere('offer.description', 'like', '%' . $key . '%');
                    })
                    ->Where('offer.status','=',1)
                    ->leftJoin('category','offer.category_id','=','category.id_category')
                    ->leftJoin('store','store.id_store','=','offer.store_id')
                    ->orderBy('offer.offer_value')
                    ->paginate(9);

                $count = count($offer_list);

                $category_list = DB::table('category')
                    ->select('category.*')
                    ->get();
            }
            if($request->sort_by == 2)
            {
                $offer_list = DB::table('offer')
                    ->select('category.name as category_name','offer.*','store.telephone','store.address', 'offer.images','store.images as store_image','store.name as store_name')
                    ->where(function($query) use ($key){
                        return $query
                            ->Where('offer.name', 'like', '%' . $key . '%')
                            ->orWhere('offer.description', 'like', '%' . $key . '%');
                    })
                    ->Where('offer.status','=',1)
                    ->leftJoin('category','offer.category_id','=','category.id_category')
                    ->leftJoin('store','store.id_store','=','offer.store_id')
                    ->orderBy('offer.offer_value','DESC')
                    ->paginate(9);

                $count = count($offer_list);

                $category_list = DB::table('category')
                    ->select('category.*')
                    ->get();
            }
        }

        if($request->categories != 0 && $request->categories != null )
        {
            $category_id = $request->categories;

            $offer_list = DB::table('offer')
                ->select('category.name as category_name','offer.*','store.telephone','store.address', 'offer.images','store.images as store_image','store.name as store_name')
                ->where('offer.category_id','=',$category_id)
                ->Where('offer.status','=',1)
                ->where(function($query) use ($key){
                    return $query
                        ->Where('offer.name', 'like', '%' . $key . '%')
                        ->orWhere('offer.description', 'like', '%' . $key . '%');
                })
                ->leftJoin('category','offer.category_id','=','category.id_category')
                ->leftJoin('store','store.id_store','=','offer.store_id')
                ->paginate(9);

            $count = count($offer_list);

            $category_list = DB::table('category')
                ->select('category.*')
                ->get();

            if(isset($request->sort_by))
            {
                if($request->sort_by == 1)
                {
                    $offer_list = DB::table('offer')
                        ->select('category.name as category_name','offer.*','store.telephone','store.address', 'offer.images','store.images as store_image','store.name as store_name')
                        ->where('offer.category_id','=',$request->categories)
                        ->Where('offer.status','=',1)
                        ->where(function($query) use ($key){
                            return $query
                                ->Where('offer.name', 'like', '%' . $key . '%')
                                ->orWhere('offer.description', 'like', '%' . $key . '%');
                        })
                        ->leftJoin('category','offer.category_id','=','category.id_category')
                        ->leftJoin('store','store.id_store','=','offer.store_id')
                        ->orderBy('offer.offer_value')
                        ->paginate(9);

                    $count = count($offer_list);

                    $category_list = DB::table('category')
                        ->select('category.*')
                        ->get();
                }
                if($request->sort_by == 2)
                {
                    $offer_list = DB::table('offer')
                        ->select('category.name as category_name','offer.*','store.telephone','store.address', 'offer.images','store.images as store_image','store.name as store_name')
                        ->where('offer.category_id','=',$request->categories)
                        ->Where('offer.status','=',1)
                        ->where(function($query) use ($key){
                            return $query
                                ->Where('offer.name', 'like', '%' . $key . '%')
                                ->orWhere('offer.description', 'like', '%' . $key . '%');
                        })
                        ->leftJoin('category','offer.category_id','=','category.id_category')
                        ->leftJoin('store','store.id_store','=','offer.store_id')
                        ->orderBy('offer.offer_value','DESC')
                        ->paginate(9);

                    $count = count($offer_list);

                    $category_list = DB::table('category')
                        ->select('category.*')
                        ->get();
                }


            }

        }



        return view('search',compact('offer_list','count','category_list'));
    }

    public function city($id)
    {
        $data = DB::table('cities')->where('district_id',$id)->pluck('name_en','id');
        return json_encode($data);
    }

}
