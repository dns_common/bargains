<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ShopController extends Controller
{
    public function shop_index($id)
    {
        $rate_data = DB::table('rate')
            ->leftjoin('user', 'rate.user_id', 'user.id_user')
            ->select('rate.*', 'user.*')
            ->where('rate.store_id', $id)->get();

        $tot_rate=DB::table('rate')->wherestore_id($id)->sum('rate');

        if(count($rate_data)>0){
            $average_rate=$tot_rate/count($rate_data);
        }else{
            $average_rate=0;
        }

        $data = [
            'result' => DB::table('system_settings')->wherestatus(1)->get(),
            'store' => DB::table('store')->whereid_store($id)->first(),
            'offers' => DB::table('offer')->wherestore_id($id)->wherestatus(1)->get(),
            'avg_rating'=>$average_rate,
            'rate_data'=>$rate_data,
            'open_days'=>DB::table('opening_time')->wherestore_id($id)->get(),
        ];

        return view('store.store_view')->with($data);
    }
}
