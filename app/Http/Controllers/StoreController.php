<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

class StoreController extends Controller
{
    public function stores(Request $request)
    {

        $store_list = DB::table('store')
            ->select('store.*')
            ->Where('store.status','=',1)
            ->orderBy('store.created_at','desc')
            ->paginate(9);

        $count = count($store_list);

        return view('store.store_search',compact("store_list","count"));
    }

    public function store_search_page(Request $request)
    {
        $key = $request->offer_search;

        $store_list = DB::table('store')
            ->select('store.*')
            ->where(function($query) use ($key){
                return $query
                    ->Where('store.name', 'like', '%' . $key . '%')
                    ->orWhere('store.detail', 'like', '%' . $key . '%');
            })
            ->Where('store.status','=',1)
            ->paginate(9);

        $count = count($store_list);

        if(isset($request->sort_by))
        {
            if($request->sort_by == 1)
            {
                $rate_list = DB::table('rate')
                    ->select(DB::raw('sum(rate.rate)/count(rate.store_id) AS rate, rate.store_id'))
                    ->where(function($query) use ($key){
                        return $query
                            ->Where('store.name', 'like', '%' . $key . '%')
                            ->orWhere('store.detail', 'like', '%' . $key . '%');
                    })
                    ->leftjoin('store','rate.store_id','=','store.id_store')
                    ->Where('store.status','=',1)
                    ->groupBy('rate.store_id')
                    ->orderBy('rate','desc')
                    ->paginate(9);


                $count = count($rate_list);

                return view('store.store_search',compact('rate_list','count'));
            }
            if($request->sort_by == 2)
            {
                $store_list = DB::table('store')
                    ->select('store.*')
                    ->where(function($query) use ($key){
                        return $query
                            ->Where('store.name', 'like', '%' . $key . '%')
                            ->orWhere('store.detail', 'like', '%' . $key . '%');
                    })
                    ->Where('store.status','=',1)
                    ->orderBy('store.created_at','desc')
                    ->paginate(9);

                $count = count($store_list);
            }
        }


        return view('store.store_search',compact('store_list','count'));
    }
}
