<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OfferController extends Controller
{
    public function offer_index($id)
    {
        $data = [
            'offer_data' => DB::table('offer')
                ->leftjoin('store', 'offer.store_id', 'store.id_store')
                ->select('offer.*', 'store.name as store_name', 'store.city as store_city', 'store.country as store_country', 'store.telephone', 'store.id_store as store_id', 'store.address', 'store.images as store_images')
                ->where('offer.status', 1)
                ->where('offer.id_offer', $id)
                ->first(),
            'result' => DB::table('system_settings')->wherestatus(1)->get(),
        ];
        $images = json_decode($data['offer_data']->images);

        $related_offers = DB::table('offer')
            ->leftjoin('store', 'offer.store_id', 'store.id_store')
            ->select('offer.*', 'store.name as store_name','store.images as store_images')
            ->get()
            ->random(8);


        return view('offer.offer_view', $data)->with('offer_images', $images)->with('related_offers',$related_offers);
    }

}
