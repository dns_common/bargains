<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index()
    {
        $data = [
            'result' => DB::table('system_settings')->wherestatus(1)->get(),
            'categories' => DB::table('category')->paginate(6),
            'offers' => DB::table('offer')
                ->leftJoin('store', 'offer.store_id', 'store.id_store')
                ->select('offer.*', 'offer.status as offer_status','offer.images as offer_images' ,'offer.name as offer_name', 'store.name as store_name','store.images as store_images' ,'store.*')
                ->where('offer.status', 1)
                ->where('offer.verified',1)
                ->paginate(6),
            'events' => DB::table('event')
                ->leftJoin('store', 'event.store_id', 'store.id_store')
                ->select('event.*', 'store.name as store_name','store.images as store_image','store.id_store')
                ->where('event.status',1)->paginate(6),
            'stores' => DB::table('store')->wherestatus(1)->orderBy('date_created','desc')->paginate(6),
            'banners' => DB::table('ns_banners')
                ->select('ns_banners.*')
                ->where('ns_banners.status','=',1)
                ->get()
        ];

        return view('index')->with($data);
    }


    public function all_categories()
    {
        $data=[
            'result'=>DB::table('system_settings')->wherestatus(1)->get(),
        ];

        return view('search')->with($data);
    }

    public function services_view()
    {
        $data = [
            'result' => DB::table('system_settings')->wherestatus(1)->get(),
        ];

        return view('service_view')->with($data);
    }
    public function categories()
    {
        $data = [
            'result' => DB::table('system_settings')->wherestatus(1)->get(),
        ];

        $category_list = DB::table('category')
            ->select('category.*')
            ->paginate(6);


        return view('categories.index',compact('category_list'))->with($data);

    }
    public function contact()
    {
        $data=[
            'result'=>DB::table('system_settings')->wherestatus(1)->get(),
        ];

        return view('contacts.contact')->with($data);
    }
    public function setting()
    {

    }
}
