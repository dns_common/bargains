<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@index');
Route::get('/all-categories','HomeController@all_categories');
Route::get('/services_view','HomeController@services_view');
Route::get('/categories','HomeController@categories');

Route::get('/contact','HomeController@contact');
Route::get('/all_offers','OfferController@all_offers')->name('all_offers');

Route::get('/settings','HomeController@settings');

Route::post('/search','SearchController@search')->name('search');

Route::get('/search','SearchController@search')->name('search');

Route::post("/search_page",'SearchController@search_page')->name('search_page');

Route::get("/search_page",'SearchController@search_page')->name('search_page');

Route::get('view_shop/{id}','ShopController@shop_index');
Route::get('view_offer/{id}','OfferController@offer_index');

Route::get('/aboutUs', function (){
    return view('aboutUs');
});


Route::get('/city/{id}', 'SearchController@city')->name('city');

Route::get('/stores','StoreController@stores')->name('stores');

Route::post('/store_search_page','StoreController@store_search_page')->name('store_search_page');

Route::get('/store_search_page','StoreController@store_search_page')->name('store_search_page');
